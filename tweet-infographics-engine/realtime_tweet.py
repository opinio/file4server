#!/usr/bin/python
# -*- coding: utf-8 -*-

from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import json

import sys
from keys import *

reload(sys)
sys.setdefaultencoding('utf8')


key = key5

consumer_key = key5.consumer_key
consumer_secret = key5.consumer_secret
access_token = key5.access_token
access_token_secret = key5.access_token_secret




class StdOutListener(StreamListener):

    count = 0
    csv = open('cwc2015final.csv','a')

    def on_data(self, data):
        self.count += 1
        tweet = json.loads(data)

        filter_data = str(self.count)+'\t'+str(tweet['id'])+'\t'+tweet['user']['id_str']+'\t'+tweet['user']['name'].replace('\t',' ').replace('\n',' ')+'\t'+tweet['user']['location'].replace('\t',' ').replace('\n',' ')+'\t'+tweet['text'].encode('utf-8','strict').strip().replace('\r', ' ').replace('\t',' ').replace('\n',' ')+'\t'+ tweet['created_at']+'\n'

        # print filter_data
        self.csv.write(filter_data)

        # selfcsv.close()

        return True

    def on_error(self, status):
        print status


if __name__ == '__main__':

    l = StdOutListener()
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    stream = Stream(auth, l)

    stream.filter(track=['#cwc2015','#AUSvsNZ','#NZvsAUS','#aussies','#blackcaps','#worldcup2015','#icc','#PlayOfTheDay','#AskCaptain','#AskTheCaptain','#ICCWorldCup2015','#CricketWorldCup','#2015WorldCup','#CricketWorldCup2015','#ICCCricketWorldCup2015','#AUSvNZL','#MCG','#BrendonMccullum','#Mccullum','#AaronFinch','#Finch','#TimSouthee','#JeromeTaylor','#MitchellStarc','#Starc','#JoshDavey','#StevenFinn','#MohammedShami','#Shami','#LasithMalinga','#Malinga','#DanielVettori','#Vettori','#NZ','#BackTheBlackCaps','#AUS','#GoGold','#goaussie', '#cwc15final'] )

