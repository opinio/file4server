#!/usr/bin/python

import re
from urllib2 import urlopen

data = open('Tweets/startup.csv').readlines()
csv = open('csv/startup.csv','w')

for a_data in data[:25]:
	
	try:
		user_id = a_data.split('\t')[3]
		
		page = 'https://twitter.com/intent/user?user_id='+user_id
		html = urlopen(page).read()
		username = re.findall(r'<a class="alternate-context" href="/(.*?)">View .*? full profile',html)[0]
		page = 'https://twitter.com/'+ username
		html = urlopen(page).read()
		try:
			location = re.findall(r'<span class="ProfileHeaderCard-locationText u-dir" dir="ltr">(.*?)</span>',html)[0]
		except Exception:
			location = ''
		
		print user_id
		print page
		print location
		
		csv.write(str(user_id)+'\t'+username.replace('\t',' ').replace('\n',' ')+'\t'+location.replace('\t',' ').replace('\n',' ')+'\n')

	except Exception:
		pass

csv.close()
