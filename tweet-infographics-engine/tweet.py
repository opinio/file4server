from TwitterSearch import *
import sys
from keys import *

# -*- coding: utf-8 -*-
reload(sys)
sys.setdefaultencoding('utf8')

tso = TwitterSearchOrder()
#tso.set_keywords(['iamwithdrkc'])
tso.set_keywords(['#cwc2015'])
tso.set_keywords(['#AUSvsNZ'])
tso.set_keywords(['#aussies'])
tso.set_keywords(['#blackcaps'])
#tso.set_language('en')
#tso.set_include_entities(False)
tso.set_count(100)

key = key1
ts = TwitterSearch(
	key.consumer_key,
	key.consumer_secret,
	key.access_token,
	key.access_token_secret
)

csv = open('Tweets/cwc2015.csv','a')

i=0
for tweet in ts.search_tweets_iterable(tso):
	
	i+=1
	print i
	
	csv.write(
		str(i)+'\t'+
		str(tweet['id'])+'\t'+
		tweet['user']['id_str']+'\t'+
		tweet['user']['name'].replace('\t',' ').replace('\n',' ')+'\t'+
		tweet['user']['location'].replace('\t',' ').replace('\n',' ')+'\t'+
		tweet['text'].replace('\t',' ').replace('\n',' ')+'\t'+
		tweet['created_at']+'\n'
	)

csv.close()
